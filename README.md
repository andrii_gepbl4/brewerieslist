﻿# The brewery application documentation

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Package structure](#packages-structure)
* [Contacts](#contacts)

## General info 
The lightweight example of REST api with MVVM architecure and This application is a lightweight example of how to use the REST API approach with MVVM architecture according to [Google's docs](https://developer.android.com/jetpack/docs/guide). 

## Technologies
- [View Binding](https://developer.android.com/topic/libraries/view-binding) - is for injecting views into the activity/fragment clases.
- [Dagger](https://github.com/google/dagger) - is for dependencies injections.
- [Room](https://developer.android.com/topic/libraries/architecture/room) - the SQL wrapper, which gives better access to SQL database.
- [Retrofit](https://square.github.io/retrofit/) the type safe HTTP client.
- [Gson](https://github.com/google/gson) for converting [JSON](https://www.json.org/json-en.html) to java object and vice versa.
- [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) - is an observable data holder class.

## Packages structure
- **API** has a service to fetch data
- **Dagger** package for self the self-titled library.
- **Database** is for Room.
- **Model** for data classes.
- **Repository** holds Google's MVVM repositories.
- **UI** for activities and fragments. 

## Contacts
