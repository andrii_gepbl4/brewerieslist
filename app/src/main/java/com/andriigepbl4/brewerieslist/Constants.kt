package com.andriigepbl4.brewerieslist

class Constants {
    companion object{
        val DATABASE_NAME = "brew_database.db"
        val BASE_URL = "https://api.openbrewerydb.org/"
        val TAG = "BREWTAG"
    }
}