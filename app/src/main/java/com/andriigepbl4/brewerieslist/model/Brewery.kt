package com.andriigepbl4.brewerieslist.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity (tableName = "brew_table")
data class Brewery(
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "brew_id")
    var id: String = "",
    var name:String = "",
    var street:String = "",
    var city:String = "",
    var state:String = "",
    var country:String = "",
    var longitude:String = "",
    var latitude:String = "",
    var phone:String = "",
    @SerializedName("website_url")
    var websiteUrl: String =""
)