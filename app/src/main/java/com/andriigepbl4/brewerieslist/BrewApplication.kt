package com.andriigepbl4.brewerieslist

import android.app.Application
import com.andriigepbl4.brewerieslist.dagger.ApplicationComponent
import com.andriigepbl4.brewerieslist.dagger.DaggerApplicationComponent
import com.andriigepbl4.brewerieslist.database.BrewRoomDatabase

class BrewApplication : Application() {
    val appComponent: ApplicationComponent = DaggerApplicationComponent.create()

    companion object {
        lateinit var database: BrewRoomDatabase
    }

    override fun onCreate() {
        super.onCreate()
        database = BrewRoomDatabase.getDataBase(this)
    }
}