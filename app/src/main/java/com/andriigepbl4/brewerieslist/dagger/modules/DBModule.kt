package com.andriigepbl4.brewerieslist.dagger.modules

import com.andriigepbl4.brewerieslist.BrewApplication
import com.andriigepbl4.brewerieslist.api.BreweryApiService
import com.andriigepbl4.brewerieslist.database.dao.BreweryDAO
import com.andriigepbl4.brewerieslist.repository.BrewRepository
import dagger.Module
import dagger.Provides

@Module
class DBModule {
    @Provides
    fun getBreweryDao(): BreweryDAO = BrewApplication.database.brewDao()

    @Provides
    fun getBrewService(): BreweryApiService = BreweryApiService.create()

    @Provides
    fun getBrewRepository(brewDAO: BreweryDAO, brewService: BreweryApiService) =
        BrewRepository(brewDAO, brewService)
}