package com.andriigepbl4.brewerieslist.dagger

import com.andriigepbl4.brewerieslist.ui.MainActivity
import com.andriigepbl4.brewerieslist.dagger.modules.AppModule
import com.andriigepbl4.brewerieslist.dagger.modules.DBModule
import com.andriigepbl4.brewerieslist.dagger.modules.ViewModuleModule
import com.andriigepbl4.brewerieslist.ui.brewlist.BrewListFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, DBModule::class, ViewModuleModule::class])
interface ApplicationComponent {
    fun inject(activity: MainActivity)
    fun inject(fragment: BrewListFragment)
}