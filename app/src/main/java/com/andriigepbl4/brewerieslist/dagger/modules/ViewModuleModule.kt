package com.andriigepbl4.brewerieslist.dagger.modules

import androidx.lifecycle.ViewModel
import com.andriigepbl4.brewerieslist.ui.brewlist.BrewListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModuleModule {
    @Binds
    @IntoMap
    @ViewModelKey(BrewListViewModel::class)
    fun model(model: BrewListViewModel): ViewModel
}