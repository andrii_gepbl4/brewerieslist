package com.andriigepbl4.brewerieslist.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.andriigepbl4.brewerieslist.Constants
import com.andriigepbl4.brewerieslist.api.BreweryApiService
import com.andriigepbl4.brewerieslist.database.dao.BreweryDAO
import com.andriigepbl4.brewerieslist.model.Brewery
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BrewRepository @Inject constructor(
    private val brewDAO: BreweryDAO,
    private val brewService: BreweryApiService
) {
    fun getAllBreweries(): LiveData<List<Brewery>> {
        val cached: LiveData<List<Brewery>> = getListFromDB()
        if (!cached.value.isNullOrEmpty()) {
            return cached
        }
        return getAllBreweriesFromService()
    }

    fun getBreweriesByName(name: String): MutableLiveData<List<Brewery>> {
        val data = MutableLiveData<List<Brewery>>()
        brewService.getBreweryByName(name).enqueue(object : Callback<List<Brewery>> {
            override fun onResponse(call: Call<List<Brewery>>, response: Response<List<Brewery>>) {
                data.value = response.body()!!
            }
            override fun onFailure(call: Call<List<Brewery>>, t: Throwable) {
                logError(t)
            }
        })
        return data
    }

    private fun getAllBreweriesFromService() : LiveData<List<Brewery>> {
        val data = MutableLiveData<List<Brewery>>()
        brewService.getBreweriesList().enqueue(object : Callback<List<Brewery>> {
            override fun onResponse(call: Call<List<Brewery>>, response: Response<List<Brewery>>) {
                val brewList = response.body()
                data.value = brewList
                if (brewList != null) {
                    saveListToDB(brewList)
                }
            }
            override fun onFailure(call: Call<List<Brewery>>, t: Throwable) {
                logError(t)
            }
        })
        return data
    }

    private fun saveListToDB(brewList: List<Brewery>) {
        runBlocking {
            brewDAO.inserAll(brewList)
        }
    }

    private fun getListFromDB(): LiveData<List<Brewery>> {
        return runBlocking {
            brewDAO.getAll()
        }
    }

    private fun logError(t: Throwable) {
        Log.d(Constants.TAG, "Error $t")
    }
}