package com.andriigepbl4.brewerieslist.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.andriigepbl4.brewerieslist.R
import com.andriigepbl4.brewerieslist.model.Brewery

class BrewRecyclerAdapter(private val brewList: List<Brewery>) :
    RecyclerView.Adapter<BrewRecyclerAdapter.BrewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BrewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return BrewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return brewList.size
    }

    override fun onBindViewHolder(holder: BrewHolder, position: Int) {
        val brew: Brewery = brewList[position]
        holder.bind(brew)
    }

    class BrewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_brew, parent, false)),
        View.OnClickListener {
        private val tvInfo: TextView = itemView.findViewById(R.id.tvInfo)

        fun bind(brewery: Brewery) {
            tvInfo.text = toString(brewery)
        }

        override fun onClick(v: View?) {

        }

        private fun toString(brewery: Brewery) : String{
            return "name + ${brewery.name}\n " +
                    "phone + ${brewery.phone}\n " +
                    "websiteUrl + ${brewery.websiteUrl}\n " +
                    "country + ${brewery.country}\n " +
                    "state + ${brewery.state}\n " +
                    "city + ${brewery.city}\n " +
                    "street + ${brewery.street}\n "
        }
    }
}