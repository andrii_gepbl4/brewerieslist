package com.andriigepbl4.brewerieslist.api

import com.andriigepbl4.brewerieslist.Constants
import com.andriigepbl4.brewerieslist.model.Brewery
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface BreweryApiService {
    @GET("breweries/")
    fun getBreweriesList(): Call<List<Brewery>>

    @GET("breweries/")
    fun getBreweryByName(@Query("by_name") name: String): Call<List<Brewery>>

    companion object Factory {
        fun create(): BreweryApiService {
            val retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

            return retrofit.create(BreweryApiService::class.java)
        }
    }
}