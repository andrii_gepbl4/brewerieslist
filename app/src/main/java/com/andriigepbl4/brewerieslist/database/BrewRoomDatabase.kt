package com.andriigepbl4.brewerieslist.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.andriigepbl4.brewerieslist.Constants
import com.andriigepbl4.brewerieslist.database.dao.BreweryDAO
import com.andriigepbl4.brewerieslist.model.Brewery

@Database(entities = arrayOf(Brewery::class), version = 1, exportSchema = false)
abstract class BrewRoomDatabase : RoomDatabase(){
    abstract fun brewDao(): BreweryDAO

    companion object{
        @Volatile
        private var INSTANCE : BrewRoomDatabase? = null

        fun getDataBase(context: Context) : BrewRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    BrewRoomDatabase::class.java,
                    Constants.DATABASE_NAME
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}