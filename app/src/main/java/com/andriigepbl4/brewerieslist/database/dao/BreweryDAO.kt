package com.andriigepbl4.brewerieslist.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.andriigepbl4.brewerieslist.model.Brewery

@Dao
interface BreweryDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(brewery: Brewery)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun inserAll(breweryList: List<Brewery>)

    @Delete
    suspend fun delete(brewery: Brewery)

    @Query("SELECT * FROM brew_table")
    fun getAll(): LiveData<List<Brewery>>

    @Query("SELECT * FROM brew_table WHERE brew_id = :brewId")
    fun getBreweryByID(brewId: String): LiveData<Brewery>
}