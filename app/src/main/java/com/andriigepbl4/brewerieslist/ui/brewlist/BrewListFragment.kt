package com.andriigepbl4.brewerieslist.ui.brewlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.andriigepbl4.brewerieslist.databinding.BrewListFragmentBinding
import com.andriigepbl4.brewerieslist.model.Brewery
import com.andriigepbl4.brewerieslist.ui.MainActivity
import com.andriigepbl4.brewerieslist.view.adapters.BrewRecyclerAdapter
import com.andriigepbl4.brewerieslist.viewmodel.ViewModelFactory
import javax.inject.Inject

class BrewListFragment : Fragment() {
    companion object {
        fun newInstance() =
            BrewListFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: BrewListViewModel
    private lateinit var binding: BrewListFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = BrewListFragmentBinding.inflate(inflater, container, false)
        (activity as MainActivity).component.inject(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(BrewListViewModel::class.java)
        viewModel.prepareBrewList()
        observeBrewList()
    }

    private fun observeBrewList() {
        viewModel.getBrewList().observe(viewLifecycleOwner, Observer<List<Brewery>> { brewList ->
            binding.rvBrewList.also {
                it.layoutManager = LinearLayoutManager(context)
                it.setHasFixedSize(true)
                it.adapter = BrewRecyclerAdapter(brewList)
            }
        })
    }
}