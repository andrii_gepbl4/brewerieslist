package com.andriigepbl4.brewerieslist.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.andriigepbl4.brewerieslist.BrewApplication
import com.andriigepbl4.brewerieslist.R
import com.andriigepbl4.brewerieslist.dagger.ApplicationComponent
import com.andriigepbl4.brewerieslist.ui.brewlist.BrewListFragment

class MainActivity : AppCompatActivity() {
    lateinit var component: ApplicationComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        component = (applicationContext as BrewApplication).appComponent
        component.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startFragment()
    }

    private fun startFragment() {
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragment_container, BrewListFragment.newInstance())
        transaction.commit()
    }
}
