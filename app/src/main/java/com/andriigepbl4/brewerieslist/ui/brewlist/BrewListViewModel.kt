package com.andriigepbl4.brewerieslist.ui.brewlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.andriigepbl4.brewerieslist.model.Brewery
import com.andriigepbl4.brewerieslist.repository.BrewRepository
import com.andriigepbl4.brewerieslist.utils.Coroutines
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class BrewListViewModel @Inject constructor(
    private val brewRepo: BrewRepository
) : ViewModel() {
    private lateinit var brewList: LiveData<List<Brewery>>
    private lateinit var job: Job

    fun getBrewList(): LiveData<List<Brewery>> {
         brewList = runBlocking {
            brewRepo.getAllBreweries()
        }
        return brewList
    }

    fun prepareBrewList() {
        job = Coroutines.ioThenMain(
            { brewRepo.getAllBreweries() },
            { brewList = it!! }
        )
    }

    override fun onCleared() {
        super.onCleared()
        if(::job.isInitialized) job.cancel()
    }
}